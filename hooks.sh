#!/bin/bash

gp await-port 23000
_repos=(
  devops
)
<<repos
  datapipeline
  #looker-analytics
  #business
  #db-analytics
repos

for _repo in "${_repos[@]}"; do {
  echo "Cloning $_repo..."
  git clone https://gitlab.com/textemma/"$_repo".git;
} done
_exts=(
  ms-python.python
  kylepaulsen.stretchy-spaces
  redhat.vscode-xml
  redhat.vscode-yaml
  esbenp.prettier-vscode
  ms-kubernetes-tools.vscode-kubernetes-tools
  reduckted.vscode-gitweblinks
  eamodio.gitlens
  huizhou.githd
  ms-azuretools.vscode-docker
  hashicorp.terraform
  humao.rest-client
)
for _ext in "${_exts[@]}"; do {
   code --install-extension "$_ext";
} done
